#!/bin/bash
# Create Debian image for a H. server
# GPL v3 or later - alcalina (A) riseup.net

set -x

export LANG=C

source lib/strapon.conf
source strapon-env.conf
source lib/shred.func.sh
source lib/partitions.func.sh
source lib/raid.func.sh
source lib/luks.func.sh
source lib/lvm.func.sh
source lib/filesystem.func.sh
source lib/bootstrap.func.sh
source lib/ssh.func.sh

checkMandatoryVariables() {
  if [ -z "${LUKS_PASSPHRASE}" ] ; then
    echo "Error: please define a LUKS_PASSPHRASE."
    exit 1
  fi
  if [ -z "${SSH_PUBKEY}" ] ; then
    echo "Error: please define a SSH_PUBKEY."
    exit 1
  fi
}

installPackages() {
   export DEBIAN_FRONTEND=noninteractive
   apt update
   apt install -y parted mdadm cryptsetup lvm2 fakeroot debootstrap
}

main() {
   installPackages
   assembleRAID
   mkdir -p /tmp/target/boot
   cryptsetup luksOpen /dev/md0 ${VG_NAME}_crypt
   vgchange -ay ${VG_NAME}
   mount /dev/${VG_NAME}/root /tmp/target
   mount /dev/sda2 /tmp/target/boot 
   #@mountFilesystems
   bindFilesystems
}

main
