#!/bin/bash

# This script is meant to leave the system in a state where strapon.sh can be
# run again. For that, it tries to undo everything the strapon.sh script does,
# in reverse order.

source lib/strapon.conf
source strapon-env.conf
source lib/filesystem.func.sh
source lib/lvm.func.sh
source lib/luks.func.sh
source lib/raid.func.sh
source lib/bootstrap.func.sh

unbindFilesystems
teardownFilesystems
teardownLVM
teardownLUKS
teardownRAID
