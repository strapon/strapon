#!/bin/bash
#

set -ex

DEP="jq"

if [ ${#} == 0 ]; then
	echo "Nenhum argumento encontrado."
        echo  "Use [-d] para instalar dependências; [-c] checar espaço disponível";
fi


apt_install() {
    apt update
    apt install ${DEP}
}

pvs_check() {
    pvs --reportformat json | jq '.report'
}

create_lv() {
   if ! [ -L /dev/${3}/${2} ]; then 
       lvcreate -L $1 -n $2 $3;
   else 
       echo "Device exist.";
   fi
}

virt_install() {
   virt-install --name=${1}  \
   --memory=${2} \
   --location="http://ftp.us.debian.org/debian/dists/buster/main/installer-amd64" \
   --virt-type="kvm" \
   --graphics="none" \
   --disk path=/dev/${3}/${1} \
   --extra-args="console=ttyS0" \
   --os-variant="debian7"
}


while getopts "dcl" opt; do
  case $opt in
    d) echo "Install dependencies"
       apt_install;;
    c) echo "Checking available space"
       pvs_check;;
    l) echo "Create logical volume and run virt-install"
       DEV="tiui"
       SIZE="40G"
       NAME="araponga"
       MEM="2048"
       create_lv ${SIZE} ${NAME} ${DEV};
       virt_install ${NAME} ${MEM} ${DEV};;
  esac
done

