setupRAID() {
  apt install mdadm
  mdadm --stop --scan
  createRAID
}

assembleRAID() {
  mdadm --assemble --scan
}

createRAID() {  
  partitions=""
  for d in ${DISKS}; do
    if [ ${d:0:9} == '/dev/nvme' ]; then p="${d}p3"; else p="${d}3"; fi;
    partitions="${partitions} ${p}"
    mdadm --zero-superblock ${p}
  done
  raid_devices=$( echo ${DISKS} | wc -w )
  mdadm --create --verbose ${RAID_VOLUME} --level=${RAID_LEVEL} --raid-devices=${raid_devices} --force --run --auto=yes ${partitions}
}

teardownRAID() {
  mdadm --stop ${RAID_VOLUME}
}
