setupFilesystems(){
  createFilesystems
  mkdir -p ${TARGET}
  mountFilesystems
}

createFilesystems() {
  for d in ${DISKS}; do
      if [ ${d:0:9} == '/dev/nvme' ]; then p="${d}p2"; else p="${d}2"; fi
      mkfs.ext2 -F ${p}
  done
  mkfs.ext4 -F /dev/${VG_NAME}/root
  mkswap /dev/${VG_NAME}/swap
}

mountFilesystems(){
  mount /dev/${VG_NAME}/root ${TARGET}
  mkdir -p ${TARGET}/boot
  d=(${DISKS})
  if [ ${d[0]:0:9} == '/dev/nvme' ]; then p="${d[0]}p2"; else p="${d[0]}2"; fi
  mount ${p} ${TARGET}/boot
}

teardownFilesystems() {
  for disk in ${DISKS}; do
    umount ${TARGET}/boot-${disk:5}2
  done
  umount ${TARGET}
}
