setupPartitions() {
  for d in ${DISKS}; do
    parted -s -a optimal ${d} -- mklabel gpt \
      mkpart primary fat32 1MiB 2MiB \
      mkpart primary ext2 2MiB 1GiB \
      mkpart primary ext2 1GiB 100% \
      set 1 bios_grub on \
      set 3 raid on \
      name 1 efi \
      name 2 linux-boot \
      name 2 linux-raid-1
    parted ${d} align-check optimal 1 || exit 1  # die if partitioning fails
    parted ${d} print
  done
}
