setupLUKS() {
  echo ${LUKS_PASSPHRASE} | cryptsetup --batch-mode luksFormat ${RAID_VOLUME}
  openLUKS
}

openLUKS() {
  echo ${LUKS_PASSPHRASE} | cryptsetup luksOpen ${RAID_VOLUME} ${VG_NAME}
}

teardownLUKS() {
  cryptsetup luksClose ${VG_NAME}
}
