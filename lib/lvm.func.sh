setupLVM() {
  createLVM
}

createLVM() {
  pvcreate -f --dataalignment=512k /dev/mapper/${VG_NAME}
  vgcreate ${VG_NAME} /dev/mapper/${VG_NAME}
  lvcreate -L ${SWAP_SIZE} -n swap ${VG_NAME}
  lvcreate -L ${ROOT_SIZE} -n root ${VG_NAME}
}

teardownLVM() {
  lvchange -an ${VG_NAME}
  vgchange -an ${VG_NAME}
  pvchange -xn /dev/mapper/${VG_NAME}
}
