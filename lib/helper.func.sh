#!/bin/bash

DISKS=($(lsblk -l  | grep 'disk' | cut -d" " -f1))
DISKS_LEN=${#DISKS[@]} # Count disks to further define RAID_LEVEL
IFACE=${IFACE:-$( ip -4 -o addr show up | cut -d" " -f2 | grep -v '^lo$' | head -n 1 )}
IP=${IP:-$(ip -o -4  addr show up dev ${iface} | grep 'inet' | cut -d" " -f7 | sed 's/\/.*$//')}
GATEWAY=${GATEWAY:-$(ip -o -4 route show dev ${iface} | cut -d" " -f3 | head -n1)}
NETMASK=${NETMASK:-/$(ip -o -4  addr show up dev ${iface} | grep 'inet' | cut -d" " -f7 | sed 's/.*\///')}
