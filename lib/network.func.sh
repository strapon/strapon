# See: https://wiki.debian.org/NetworkInterfaceNames
predictNetworkInterfaceName() {
	output=( )
	# predict for the first interface that is not "lo"
	iface=$( ip -4 -o a | cut -d" " -f2 | grep -v '^lo$' | head -n 1 )
	while read l; do
		key=$( echo $l | sed -e "s/=.*//" )
		val=$( echo $l | sed -e "s/.*=//" )
		output[${key}]="${val}"
	done < <( udevadm test-builtin net_id /sys/class/net/${iface} 2>/dev/null | grep ID_NET_NAME )
        preferences=(
		ID_NET_NAME_FROM_DATABASE
		ID_NET_NAME_ONBOARD
		ID_NET_NAME_SLOT
		ID_NET_NAME_PATH
		ID_NET_NAME_MAC
	)
	for item in ${preferences[*]}; do
		if [ -n "${output[${item}]}" ]; then
			prediction=${output[${item}]}
			break
		fi
	done
	echo ${prediction}
}

replaceNetworkInterfaceName() {
	interfaceName=$( predictNetworkInterfaceName )
	sed -i "s/NET_DEVICE/${interfaceName}/g" ${TARGET}/etc/network/interfaces
}

setupNetwork() {
	replaceNetworkInterfaceName
}
