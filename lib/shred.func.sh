#!/bin/bash

shredDisk() {
  disk=${1}
  dd if=/dev/urandom of=${disk} status=progress
}

shredDisks() {
  for d in ${DISKS}; do
    shredDisk ${d}
  done
}
