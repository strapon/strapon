bootstrapSystem() {
  fakeroot debootstrap stable ${TARGET}
  bindFilesystems
  installChrootPackages
  writeFstab
  writeCrypttab
  configureNetwork
  installGrub
}

writeFstab() {
  ROOT_UUID=$( lsblk /dev/${VG_NAME}/root -o UUID | tail -n 1 )
  BOOT_UUID=$( lsblk /dev/sda2 -o UUID | tail -n 1 )
  SWAP_UUID=$( lsblk /dev/${VG_NAME}/swap -o UUID | tail -n 1 )
  sed -e s/ROOT_UUID/${ROOT_UUID}/g -e s/BOOT_UUID/${BOOT_UUID}/g -e s/SWAP_UUID/${SWAP_UUID}/g files/etc/fstab > ${TARGET}/etc/fstab
}

writeCrypttab(){
  RAID_UUID=$( lsblk /dev/md0 -o UUID | head -n 2 | tail -n 1 )
  sed -e s/VG_NAME/${VG_NAME}/g -e s/RAID_UUID/${RAID_UUID}/g files/etc/crypttab > ${TARGET}/etc/crypttab
}

configureNetwork() {
  escaped=$( echo ${IP} | sed -e "s/\//\\\\\\//" )
  sed -e "s/IP/${escaped}/g" -e "s/GATEWAY/${GATEWAY}/g" files/etc/network/interfaces > ${TARGET}/etc/network/interfaces
  echo ${VG_NAME} > ${TARGET}/etc/hostname
}

installChrootPackages() {
  export DEBIAN_FRONTEND=noninteractive
  chroot ${TARGET} apt install -y \
    mdadm \
    cryptsetup \
    lvm2 \
    linux-image-amd64 \
    grub-efi-amd64 \
    grub-pc-bin \
    dropbear \
    busybox \
    openssh-server
}

bindFilesystems() {
  mkdir -p ${TARGET}/run/udev
  for dir in dev sys run/udev; do
    mount -o bind /${dir} ${TARGET}/${dir}
  done
  mount -t proc none ${TARGET}/proc
}

unbindFilesystems() {
  umount ${TARGET}/proc
  for dir in dev sys run/udev; do
    umount ${TARGET}/${dir}
  done
}

installGrub(){
  bareip=$( echo ${IP} | sed -e "s/\/..//" )
  sed -e "s/IP/${bareip}/g" -e "s/NETMASK/${NETMASK}/g" -e "s/GATEWAY/${GATEWAY}/g" files/etc/default/grub > ${TARGET}/etc/default/grub
  chroot ${TARGET} update-grub
  for disk in ${DISKS}; do
     chroot ${TARGET} grub-install ${disk}
  done
  echo ${SSH_PUBKEY} > ${TARGET}/etc/dropbear-initramfs/authorized_keys
  sed -e "s/DROPBEAR_PORT/${DROPBEAR_PORT}/g" files/etc/dropbear-initramfs/config > ${TARGET}/etc/dropbear-initramfs/config
  chroot ${TARGET} update-initramfs -uk all
}
