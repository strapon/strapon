printSSHInfo() {
  printHostKeys
  printDropbearKeys
}

setAuthorizedKeys() {
  chroot ${TARGET} mkdir /root/.ssh
  chroot ${TARGET} chmod 700 /root/.ssh
  echo ${SSH_PUBKEY} > ${TARGET}/root/.ssh/authorized_keys
}

printHostKeys() {
  echo "***************"
  echo " Host SSH keys"
  echo "***************"
  chroot ${TARGET} /bin/bash -c 'for f in /etc/ssh/ssh_host_*.pub; do ssh-keygen -l -f ${f}; done'
}

printDropbearKeys() {
  echo "*******************"
  echo " Dropbear SSH keys"
  echo "*******************"
  chroot ${TARGET} /bin/bash -c 'for f in /etc/dropbear/dropbear_*_key; do dropbearkey -y -f ${f}; done'
  chroot ${TARGET} /bin/bash -c 'for i in /etc/dropbear-initramfs/dropbear_*key; do n=$( basename $i ) dropbearkey -y -f $i | grep AAAA > /tmp/$n.pub; ssh-keygen -l -f /tmp/$n.pub; done'
}
