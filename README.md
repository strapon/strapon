# strapon

Nasty scripts to debootstrap-on

## Usage

```
export LUKS_PASSPHRASE=123       # Passphrase used for full disk encryption.
export VG_NAME=hostname          # The hostname of the server.
export IP=1.2.3.4/24             # Make sure to add subnet mask after slash.
export NETMASK=255.255.255.0     # For now, we need both formats of the subnet mask.
export GATEWAY=1.2.3.1           # Used by interfaces and Grub/Dropbear
export SSH_PUBKEY="ssh-rsa ..."  # Must be RSA otherwise is not usable by Dropbear.
export DROPBEAR_PORT=2222        # Configure Dropbear SSH port.
strapon.sh
```

## Test with Vagrant

First, install vagrant.

`apt install -y vagrant`

Load the machine and run the script.

```
$ vagrant up

$ vagrant ssh

$ sudo su

# cd /vagrant
```
Don't forget to export the variables before running strapon.sh.

```
# ./strapon.sh

```

## References

    - https://gerstner.se/blog/post/encrypted_hetzner_server/
    - https://www.sysorchestra.com/hetzner-root-server-with-dual-hardware-raid-1-and-lvm-on-luks-on-debian-9/
    - https://github.com/TheReal1604
    - https://eve.gd/2012/11/02/luks-encrypting-multiple-partitions-on-debianubuntu-with-a-single-passphrase/
    - http://blogs.candoerz.com/question/330723/ubuntu-full-disc-encryption-on-hetzner-cloud-adding-add-static-route-in-initramfs.aspx
    - https://serverfault.com/questions/915118/ubuntu-full-disc-encryption-on-hetzner-cloud-adding-add-static-route-in-initramf/970566
    - https://angristan.xyz/how-to-use-encrypted-block-storage-volumes-hetzner-cloud/
    - https://blog.wbnet.dk/2015/05/31/fully-encrypted-hetzner-vx6-vserver-with-ubuntu-14-dot-04-lts/
    - https://rainbow.chard.org/2013/01/30/how-to-align-partitions-for-best-performance-using-parted/

## Missing

```
cpufreqd cpufrequtils
```
